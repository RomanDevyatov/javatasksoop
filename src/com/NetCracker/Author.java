package com.NetCracker;

public class Author {
    public String name;
    public String email;
    public char gender;

//    public Author(String name, String email, char gender){
//        this.name=name;
//        this.email=email;
//        this.gender=gender;
//    }
    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public char getGender() {
        return this.gender;
    }
}
